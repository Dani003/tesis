import React, { Component } from 'react';
import Home from './js/components/VistasPrincipales/Home.js';
import Login from './js/components/VistasPrincipales/Login.js';
import ListaUsuarios from './js/components/VistasPrincipales/ListaUsuarios.js';
import GestionEquipo from './js/components/VistasPrincipales/GestionEquipo.js';
import GestionLaboratorio from './js/components/VistasPrincipales/GestionLaboratorio.js';
import Perfil from './js/components/VistasPrincipales/Perfil.js';
import HistorialReservas from './js/components/VistasPrincipales/HistorialReservas.js';
import VisualizarReservas from './js/components/VistasPrincipales/VisualizarReservas.js';
import RealizarReservas from './js/components/VistasPrincipales/RealizarReservas.js';
import CancelarReservas from './js/components/VistasPrincipales/CancelarReservas.js';
import CalendarioVisualizar from './js/components/VistasPrincipales/CalendarioVisualizar.js';
import {BrowserRouter,Route} from 'react-router-dom';
import './css/App.css';


class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Route exact path="/" component={Home}/>
          <Route exact path="/Login" component={Login}/>
          <Route exact path="/ListaUsuarios" component={ListaUsuarios}/>
          <Route exact path="/GestionEquipo" component={GestionEquipo}/>
          <Route exact path="/GestionLaboratorio" component={GestionLaboratorio}/>
          <Route exact path="/Perfil" component={Perfil}/>
          <Route exact path="/HistorialReservas" component={HistorialReservas}/>
          <Route exact path="/VisualizarReservas" component={VisualizarReservas}/>
          <Route exact path="/RealizarReservas" component={RealizarReservas}/>
          <Route exact path="/CancelarReservas" component={CancelarReservas}/>
          <Route exact path="/CalendarioVisualizar" component={CalendarioVisualizar}/>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
