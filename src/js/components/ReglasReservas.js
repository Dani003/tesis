import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import './../../../css/App.css';
import {Row, Container, Col, Accordion, Card, Button} from 'react-bootstrap';

class Home extends Component {
  render() {
    return(
      <div className="home">
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <Container>
          <Row>
            <Col>1 of 2</Col>
            <Col>2 of 2</Col>
          </Row>
          <Row>
            <Col>1 of 3</Col>
            <Col>2 of 3</Col>
            <Col>3 of 3</Col>
          </Row>
        </Container>
        <Footer></Footer>
      </div>
    );
  }
}

export default Home;
