import React, { Component } from 'react';
import './../../css/App.css'

class LoginInfoPidi extends Component {
  render() {
    return (
        <div className='LoginInfoPidi'>
        <div className='cuadrado'></div>
          <div className=" bp3-card bp3-interactive bp3-elevation-3 CardLoginInfoPidi">
          <h2 className='TituloInfoPidi'>Programa Institucional de Fomento a la Investigación, Desarrollo e Innovación (PIDi)</h2>
            <div className='TextoInfoPidi'>
              Este programa de investigación multidisciplinaria se focaliza en el desarrollo de conocimientos con impacto directo en la sociedad el cual, mediante una gestión orientada a la productividad académica y científica de alto nivel, genera proyectos, publicaciones y otros productos de relevancia en el marco de responsabilidad social y aporte tecnológico.
            </div>
          </div>
        </div>
    );
  }
}

export default LoginInfoPidi;
//componente izquierdo del login. Aqhi va un recuadro con la imagen del pidi de fondo
// y sobre esta imagen, una capa de color transparente encima, donde habra informacion del pidi
