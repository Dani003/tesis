import React, { Component } from 'react';
import './../../css/App.css'
import { Link } from "react-router-dom";

class TopBar extends Component {
  render() {
    return (
      <div className="navBarbox2 ZIndex">
        <div className='right'>
          <div className="imagedispositivo">
            <img className="imgLogoUtem" src="http://sitios.vtte.utem.cl/principal/wp-content/uploads/sites/2/2017/03/marca-utem-horizontal.jpg"></img>
          </div>
        </div>
        <div className="left">
          <img className="imgLogoUtem" src="http://www.redciencia.net/sites/default/files/PIDI%20UTEM.png"></img>
        </div>
    </div>
    );
  }
}

export default TopBar;

//Barra en la parte superior de la pagina. En el extreno izquierdo tiene el logo de la utem yt al derecho tiene el nombre del pidi
