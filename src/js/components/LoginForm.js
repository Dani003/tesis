import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './../../css/App.css'

class LoginForm extends Component {
  render() {
    return (
        <div className='homeP'>
        <div>
        <img className="imgLogoUtemLogin" src="http://sitios.vtte.utem.cl/principal/wp-content/uploads/sites/2/2017/03/marca-utem-horizontal.jpg"></img>
        </div>
            <div class="bp3-form-group PlaceHolderLogin1">
            {/*  <label class="bp3-label PalabrasFormLogin" for="form-group-input">
                Email:
              </label>*/}
              <div class="bp3-form-content">
                <div class="bp3-input-group InputLogin">
                  <span class="bp3-icon"></span>
                  <input id="form-group-input" type="text" class="bp3-input PlaceHolderLoginForm"
                    placeholder="Ingresar Correo" dir="auto" />
                </div>
              </div>
              {/*<label class="bp3-label PalabrasFormLogin" for="form-group-input">
                Contraseña:
              </label>*/}
              <div class="bp3-form-content">
                <div class="bp3-input-group InputLogin">
                  <span class="bp3-icon"></span>
                  <input id="form-group-input" type="text" class="bp3-input PlaceHolderLoginForm "
                    placeholder="Ingresar Contraseña" dir="auto" />
                </div>
              </div>
              <Link to="/">
              <button type="button" class=" BotonIngreso">Ingresar</button>
            </Link>
            </div>
            <Link to="/">
            <p className='ColorGreen'>¿Olvido su contraseña?</p>
            </Link>
            <div class="bp3-card LoginCard">
              Para cualquier consulta envíe un correo a soporte.sisei@utem.cl
            </div>
        </div>
    );
  }
}

export default LoginForm;
//componente izquierdo del login. Aqhi va un recuadro con la imagen del pidi de fondo
// y sobre esta imagen, una capa de color transparente encima, donde habra informacion del pidi
