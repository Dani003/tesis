import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import FirstButton from './../FirstButton.js';
import SecondButton from './../SecondButton.js';
import ThirdButton from './../ThirdButton.js';
import Footer from './../Footer.js';
import './../../../css/App.css';
import { Link } from "react-router-dom";
import {Row, Container, Col, Accordion, Card, Button} from 'react-bootstrap';

class Home extends Component {
  render() {
    return(
      <div className="App">
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <div className='BotonesHorario'>
          <Link to="/VisualizarReservas">
              <FirstButton></FirstButton>
          </Link>
          <Link to="/RealizarReservas">
            <SecondButton></SecondButton>
          </Link>
          <Link to="/CancelarReservas">
            <ThirdButton></ThirdButton>
          </Link>
        </div>
      </div>
    );
  }
}

export default Home;
