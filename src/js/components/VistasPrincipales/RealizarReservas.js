import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import FormRealizarReserva from './../FormRealizarReserva.js';
import './../../../css/App.css'


class GestionEquipo extends Component {
  render() {
    return (
      <div>
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <FormRealizarReserva></FormRealizarReserva>
      </div>
    );
  }
}
export default GestionEquipo;
