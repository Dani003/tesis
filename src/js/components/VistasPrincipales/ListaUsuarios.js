import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Usuario from './../Usuario.js';
import Footer from './../Footer.js';
import CrearUsuario from './../CrearUsuario.js';
import './../../../css/App.css'

class ListaUsuarios extends Component {
  render() {
    return (
      <div className="App">
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <CrearUsuario></CrearUsuario>
        <Usuario></Usuario>
        <Footer></Footer>
      </div>
    );
  }
}

export default ListaUsuarios;
