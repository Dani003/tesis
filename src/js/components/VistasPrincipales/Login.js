import React, { Component } from 'react';
import LoginForm from './../LoginForm.js';
import LoginInfoPidi from './../LoginInfoPidi.js';
import TopBar from './../TopBar.js';
import './../../../css/App.css'

class Login extends Component {
  render() {
    return (
      <div>
        <div className='wrapperLogin'>
          <LoginInfoPidi></LoginInfoPidi>
          <LoginForm></LoginForm>
        </div>
      </div>
    );
  }
}

export default Login;
