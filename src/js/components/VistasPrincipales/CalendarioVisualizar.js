import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import FormRealizarReserva from './../FormRealizarReserva.js';
import CalendarioVisualizarComp from './../CalendarioVisualizarComp.js';
import './../../../css/App.css'


class CalendarioVisualizar extends Component {
  render() {
    return (
      <div>
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <CalendarioVisualizarComp></CalendarioVisualizarComp>
      </div>
    );
  }
}
export default CalendarioVisualizar;
