import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import ListaHistorial from './../ListaHistorial.js';
import './../../../css/App.css'


class HistorialReservas extends Component {
  render() {
    return (
      <div>
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <ListaHistorial></ListaHistorial>
        <Footer></Footer>
      </div>
    );
  }
}

export default HistorialReservas;
