import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import GestionPerfil from './../GestionPerfil.js';
import './../../../css/App.css'


class Perfil extends Component {
  render() {
    return (
      <div>
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <GestionPerfil></GestionPerfil>
        <Footer></Footer>
      </div>
    );
  }
}

export default Perfil;
