import React, { Component } from 'react';
import MenuBar from './../MenuBar.js';
import TopBar from './../TopBar.js';
import Footer from './../Footer.js';
import FormVisualizarReserva from './../FormVisualizarReserva.js';
import './../../../css/App.css'


class GestionEquipo extends Component {
  render() {
    return (
      <div>
        <TopBar></TopBar>
        <MenuBar></MenuBar>
        <FormVisualizarReserva></FormVisualizarReserva>
      </div>
    );
  }
}
export default GestionEquipo;
